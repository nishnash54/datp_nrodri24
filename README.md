## Setup project

	# setup directories and load datasets
	sh setup_directories.sh
	sh setup_datasets.sh

## Setup python environment

	# create environment
	conda create -n datp python==3.8.0
	conda activate datp

	# install dependencies
	pip install -r requirements.txt

	# generate dataset
 	python process_data.py --dataset holstep
 
## Training a Model

To train a model

	python train_model.py --dataset holstep --node_embedder <GCN, MPNN> --device <cuda, cpu>

Results are generated and stored in `./results/` and model artifacts are scored in `./models`

